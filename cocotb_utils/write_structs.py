import re,datetime,textwrap
from string import Template

def create_dataclass_frm_bsv(structfile_path):

    structfile = open(structfile_path , 'r')
    structinfo = structfile.read()
    structfile.close()

    timestamp = datetime.datetime.now()

    bsvstruct_file = open('test/bsvstruct_class.py' , 'w')

    replace_chars = ['.','[',']']

    bsvstruct_file.write('''
# Generated on {0}
from dataclasses import dataclass'''.format(timestamp))

    temp_setstr = '''
self.{0} = (val.integer >> {1}) & {2}'''

    temp_getstr = Template('''
val += '{0:0${width}b}'.format(self.$name & $mask)''')

    temp_fieldstr = Template('''
    '${key}' : ${width}''')

    temp_dataclass = '''
@dataclass(init=False)
class {0}:
{1}

    def set(self, val):
{2}

    def __bin__(self) -> str:
{3}

    def get(self) -> int:
        return int(self.__bin__(),base=2)

    def __int__(self) -> int:
        return int(self.__bin__(),base=2)

    def size(self) -> int:
        return {4}

'''

    # regex pattern to extract name of each subfield
    name_pattern = re.compile ('^\.(?P<name>.*?)\s+.*')

    # regex pattern to extract the size and position of each width
    size_pattern = re.compile ('.*\s+(?P<size>\d+?)\s+\[\s(?P<msb>.*?\d+?):(?P<lsb>.*?\d+?)\s+\]')

    # split in the input file from bluetcl in multiple structs
    allstructs = re.findall('--(.*?)\$\$\$\$',structinfo,re.M|re.S)

    # iterate over each struct and convert them to python dataclass
    for struct in allstructs:
        lines = struct.split('\n')
        name = lines[0]
        lines = lines[1:-1]
        subfields = ''
        setstr = ''
        getstr = "val = ''"
        fields = "fields_"+name+" = {"
        msize = 0
        for i in range(len(lines)):
            subfield = name_pattern.search(lines[i]).group('name')
            for elem in replace_chars:
                subfield = subfield.replace(elem, '_')
            sz_breakdown = size_pattern.search(lines[i])
            width = int(sz_breakdown.group('size'))
            lsb = int(sz_breakdown.group('lsb'))

            subfield = subfield if subfield else name
            msize += width
            subfields += subfield + ': int = 0\n'
            setstr += temp_setstr.format(subfield, lsb, hex((1 << width)-1))
            getstr += temp_getstr.safe_substitute(width=str(width),name=str(subfield),
                                                                        mask=hex((1 << width)-1))
            fields += temp_fieldstr.safe_substitute(width=str(width),key=str(subfield))+","
        getstr += "\nreturn val"
        fields = fields[:-1]+"}\n"
        subfields = textwrap.indent(subfields, '    ') + textwrap.indent(fields, '    ')
        setstr = textwrap.indent(setstr, '        ')
        getstr = textwrap.indent(getstr, '        ')
        bsvstruct_file.write(temp_dataclass.format(name, subfields, setstr,getstr,msize)+'\n')

    bsvstruct_file.close()

if __name__ == "__main__":
    create_dataclass_frm_bsv("./build/structinfo.txt")

