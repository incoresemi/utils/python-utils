import cocotb
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb_coverage.coverage import *
from collections import deque
import logging
from logging.handlers import RotatingFileHandler

log = cocotb.logging.getLogger("cocotb")

class get:
    def __init__(self,dut,name,module="",dataclass=None):
        self.clk = dut.CLK
        self.__name__ = name
        module = module + "." if module else ""
        setattr(self,"en",getattr(dut,module+"EN_"+name+"_get"))
        setattr(self,"rdy",getattr(dut,module+"RDY_"+name+"_get"))
        setattr(self,"get",getattr(dut,module+name+"_get"))
        self.dataclass = dataclass

    @cocotb.coroutine
    def read(self):
        if self.rdy != 1:
            log.debug("Waiting for Ready")
            yield RisingEdge(self.rdy)
        else:
            if self.dataclass :
                x = self.dataclass()
                x.set(self.get.value)
                val = x
            else:
                val = self.get
            log.debug(self.__name__+": Read value:"+str(val))
            self.en <= 1
            yield RisingEdge(self.clk)
            self.en <= 0
            yield Timer(1,units="ns")
            return val

class put:
    def __init__(self,dut,name,module="",dataclass=None):
        self.clk = dut.CLK
        self.__name__ = name
        module = module + "." if module else ""
        setattr(self,"en",getattr(dut,module+"EN_"+name+"_put"))
        setattr(self,"rdy",getattr(dut,module+"RDY_"+name+"_put"))
        setattr(self,"put",getattr(dut,module+name+"_put"))
        self.dataclass = dataclass

    @cocotb.coroutine
    def write(self,val):
        if self.rdy != 1:
            log.debug("Waiting for Ready")
            yield RisingEdge(self.rdy)
        else:
            log.debug(self.__name__+": Writing value:"+str(val))
            self.en <= 1
            self.put <= int(val)
            yield RisingEdge(self.clk)
            self.en <= 0

