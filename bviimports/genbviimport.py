from ruamel.yaml import YAML
import ruamel
import itertools

'''
Example Node in yaml:
recFNToIN:
  vmod: recFNToIN
  param:
    - expWidth
    - sigWidth
    - intWidth
  provisos:
    - Add#(sigWidth,expWidth, sz_fn)
    - Add#(1,sz_fn, sz_recfn)
  methods:
    request:
      type: ma
      ret:
      param:
        control: "`control"
        in: sz_recfn
        roundingMode: 3
        signedOut: 1
    out:
      type: mv
      ret: intWidth
      param: []
    intExceptionFlags:
      type: mv
      ret: 3
      param: []
  schedule:
    set1:
      type: CF
      pred:
        - out
        - intExceptionFlags
      succ:
        - out
        - intExceptionFlags
    set2:
      type: SB
      pred:
        - request
      succ:
        - out
        - intExceptionFlags
'''


yaml = YAML(typ="rt")
yaml.default_flow_style = False
yaml.allow_unicode = True

def load_yaml(foo):
    try:
        with open(foo, "r") as file:
            return dict(yaml.load(file))
    except ruamel.yaml.constructor.DuplicateKeyError as msg:
        logger = logging.getLogger(__name__)
        error = "\n".join(str(msg).split("\n")[2:-7])
        logger.error(error)
        raise SystemExit(1)

def get_additional_ports(node):
    out = ""
    if 'en' in node:
        out += " enable({})".format(node["en"])
    if 'clk' in node:
        out += " clocked_by({})".format(node["clk"])
    if 'rst' in node:
        out += " reset_by({})".format(node['rst'])
    return out;

def mv(name,node,port=False,pref=""):
    param = dict(node['param'])
    out_str = "\n"+pref
    if 'attr' in node:
        out_str += ("\n"+pref).join([x.strip() for x in (node["attr"]+"\n").split("\n")])
    else:
        out_str += "(*always_enabled*)\n"+pref
    ifc_template = "method Bit#({0}) {1}({2});".format(node["ret"],name,','.join(
                                ["Bit#({1}) {0}".format(x,param[x]) for x in param.keys()]))
    def_template = "method {0} {0}({1}) {2};".format(name,",".join(node['param']),
                                                get_additional_ports(node))
    if port:
        return "\n"+pref+def_template
    else:
        return out_str+ifc_template

def ma(name, node, port=False,pref=""):
    param = node['param']
    out_str = "\n"+pref
    if 'attr' in node:
        out_str += ("\n"+pref).join([x.strip() for x in (node["attr"]+"\n").split("\n")])
    else:
        out_str += "(*always_ready*)\n"+pref
    ifc_template = "method Action {0}({1});".format(name,','.join(
                                ["Bit#({1}) {0}".format(x,param[x]) for x in param.keys()]))
    def_template = "method {0}({1}) {2};".format(
            name,",".join(list(node['param'].keys())),get_additional_ports(node))
    if port:
        return "\n"+pref+def_template
    else:
        return out_str+ifc_template

def mav(name, node, port=False,pref=""):
    param = node['param']
    out_str = "\n"+pref
    if 'attr' in node:
        out_str += ("\n"+pref).join([x.strip() for x in (node["attr"]+"\n").split("\n")])
    else:
        out_str += "(*always_ready,always_enabled*)\n"+pref
    ifc_template = "method ActionValue#(Bit#({2})) {0}({1});".format(name,','.join(
                                ["Bit#({1}) {0}".format(x,param[x]) for x in param.keys()]),
                                node["ret"])
    def_template = "method {0} {0}({1}) {2};".format(
            name,",".join(list(node['param'].keys())),get_additional_ports(node))
    if port:
        return "\n"+pref+def_template
    else:
        return out_str+ifc_template


fdict = {
        "mv": mv,
        "ma": ma,
        "mav": mav
        }

def ifc(name,node,pref=""):
    interface_template = "\n"+pref+"interface Ifc_{0}#({1});".format(
            name,(",\n"+pref+"  ").join(["numeric type "+x for x in node['param']]))
    dec = ""

    for entry in node["methods"]:
        method = node["methods"][entry]
        dec += fdict[method["type"]](entry,method,False,pref=pref+"  ")
    return interface_template+dec+"\n"+pref+"endinterface: Ifc_{0}".format(name)

def write_pipeout(name,node,pref=""):
    args = ''
    ret_type = ''
    arg_names = ' '
    ret_val = ''
    if 'request' in node['methods']:
        req_node = node['methods']['request']
        args = ','.join(["Bit#({0}) {1}".format(req_node['param'][x],x) for x in req_node['param']])
        arg_names = ','.join([x for x in req_node['param']])
    count = 0
    for entry in node['methods']:
        if entry != 'request':
            ret_type += 'Bit#({}),'.format(node['methods'][entry]['ret'])
            ret_val += 'mod.{}(),'.format(entry)
            count += 1
    ret_type = "Tuple{}#(".format(count)+ret_type[:-1]+")"
    ret_val = "tuple{}(".format(count)+ret_val[:-1]+")"
    template = (('''
interface Ifc_opipe_{0};
    method Action ma_request({1});
    interface PipeOut#({2}) pipe_out;
endinterface

module [Module] mk_opipe_{0}(Ifc_opipe_{0});

    RWire#(Bool) wr_valid <- mkRWire();
    Ifc_{0} mod <- mk_{0};

    method Action ma_request#({1});
        mod.ma_request({3});
        wr_valid.wset(True);
    endmethod

    interface pipe_out = interface PipeOut
        method notEmpty = isValid(wr_valid.wget());
        method Action deq();
            noAction;
        endmethod
        method {2} first();
           return {4};
        endmethod
    endinterface;

endmodule
''').format(name,args,ret_type,arg_names,ret_val)).replace("\n","\n"+pref)
    return template

def write_pipe(name,node,pref=""):
    args = ''
    ret_type = ''
    arg_names = ' '
    ret_val = ''
    if 'request' in node['methods']:
        req_node = node['methods']['request']
        args = ','.join(["Bit#({0})".format(req_node['param'][x]) for x in req_node['param']])
        args = "Tuple{}#(".format(len(req_node['param']))+args+")"
        l = len(req_node['param'])
        arg_names = ','.join(['tpl_{0}(x)'.format(y) for y in range(1,l+1)])
    count = 0
    for entry in node['methods']:
        if entry != 'request':
            ret_type += 'Bit#({}),'.format(node['methods'][entry]['ret'])
            ret_val += 'mod.{}(),'.format(entry)
            count += 1
    ret_type = "Tuple{}#(".format(count)+ret_type[:-1]+")"
    ret_val = "tuple{}(".format(count)+ret_val[:-1]+")"
    template = (('''

module [Module] mk_pipe_{0}#(PipeOut#({1}) pipe_in)(PipeOut#({2}));

    Ifc_{0} mod <- mk_{0};

    rule rl_input_request(vreg && pipe_in.notEmpty());
        let x = pipe_in.first();
        mod.ma_request({3});
    endrule

    PipeOut#({2}) pipe_out = interface PipeOut
        method notEmpty = isValid(pipe_in.notEmpty());
        method Action deq();
            pipe_in.deq();
        endmethod
        method {2} first();
           return {4};
        endmethod
    endinterface;

    return pipe_out;
endmodule
''').format(name,args,ret_type,arg_names,ret_val)).replace("\n","\n"+pref)
    return template

def module(name,node,pref=""):
    mod_template = ((pref+'''import "BVI" {0}=
module mk_{1}(Ifc_{1}#({2}));
''').replace("\n","\n"+pref)).format(node['vmod'],name,",".join(node['param']))

    if 'preface' in node:
        mod_template += "  "+("\n"+pref+"  ").join([x.strip() for x in (node['preface']+"\n").split("\n")])

    mod_template = "\n"+mod_template
    for x in node["param"]:
        mod_template += "\n"+pref+"  "+"parameter {0} = valueOf({0});".format(x)

    for entry in node["methods"]:
        method = node["methods"][entry]
        mod_template += fdict[method["type"]](entry,method,True,pref=pref+"  ")

    if "schedule" in node:
        for entry in node["schedule"]:
            sched_node = node["schedule"][entry]
            mod_template += "\n"+pref+"  "+"schedule ({0}) {1} ({2});".format(
                    ",".join(sched_node['pred']),sched_node['type'],",".join(sched_node['succ'])
                    )
    if "paths" in node:
        for entry in itertools.product(node["paths"]["inputs"],node["paths"]["outputs"]):
            mod_template += "\n"+pref+"  "+"path({0},{1});".format(entry[0],entry[1])
    return mod_template+"\n"+pref+"endmodule: mk_{0}".format(name)


if __name__=="__main__":
    data = load_yaml("./import.yaml")
    with open("./test.bsv","w") as f:
        for entry in data:
            f.write(ifc(entry,data[entry],"  ")+ "\n")
            f.write(module(entry,data[entry],"  ")+"\n")
            # f.write(write_pipe(entry,data[entry],"  ")+"\n")
